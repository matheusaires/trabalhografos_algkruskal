package etapa3_kruskal;

import java.util.ArrayList;

public abstract class Grafo {
    public int numVertices;
    public int numArestas;
    
    public Grafo(int n){
        numVertices = n;
    }
    
    public boolean existeVertice(int v){
        return !((v < 0) || (v>numVertices-1));
    };
    
    public abstract void        addAresta(Aresta a);
    public abstract boolean     rmAresta(int u, int v);    
    public abstract boolean     addVertice();
    public abstract boolean     rmVertice(int v);
    public abstract ArrayList   vizinhos(int v);
    public abstract int         getAresta(int v, int u);
    
}
