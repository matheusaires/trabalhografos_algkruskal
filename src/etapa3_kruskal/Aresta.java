package etapa3_kruskal;

public class Aresta {
    private final int v1;
    private final int v2;
    private final int peso;

    public Aresta(int v1, int v2, int peso) {
        this.v1 = v1-1;
        this.v2 = v2-1;
        this.peso = peso;
    }

    public int getV1() {
        return v1;
    }

    public int getV2() {
        return v2;
    }

    public int getPeso() {
        return peso;
    }

    @Override
    public String toString() {
        String s;
        
        s = "Aresta(v="+v1+",u="+v2+",peso="+peso+")";
        
        return s;
    }
    
    
}
