package etapa3_kruskal;


public class Etapa3_Kruskal {

    public static void main(String[] args) {
        
        long start = System.currentTimeMillis();
        int numVertices = 1000;
        
        Aresta[] arestas = new Aresta[0];
        
        GrafoMA g = new GrafoMA(numVertices, arestas); 
        
        System.out.println(g);
        
//        System.out.println(g.arestas[3].getPeso());
        
        Kruskal k = new Kruskal(g);
        
        System.out.println(k.getArvore());
        
        
        //GRAFO LIDO POR AQUIVO .TXT
//        int arestas[][] = null;
//        
//        int r = 1;
//        try{
//            BufferedReader inData = new BufferedReader(new FileReader("grafo.txt"));
//            Scanner linha = new Scanner(inData.readLine());
//                
//            int n = linha.nextInt();
//            int m = linha.nextInt();
//            
//            g = new GrafoMA(n,m);
//            
//            arestas = new int[m][2]; 
//            
//            for(int i=0; i<m ;i++){
//                r++;
//                linha = new Scanner(inData.readLine());
//                arestas[i][0] = linha.nextInt();
//                arestas[i][1] = linha.nextInt();
//            }
//        }catch(NoSuchElementException e){
//            System.err.printf("Erro na linha %d. Formato do arquivo incorreto.\n",r+1);
//        }catch(NullPointerException e){
//            System.err.println("Arquivo incompleto. Linha "+r+1+" não encontrada.");
//        }catch(FileNotFoundException e){
//            System.err.println("Arquivo \"grafo.txt\" não encontrado");
//        }catch(IOException e){
//            System.err.println("Erro ao tentar ler arquivo \"grafo.txt\" ");
//        }catch(Exception e){
//            System.err.println(e);
//        }
        
//        Kruskal k = new Kruskal(g);
//        
//        System.out.println(g);
//        
//        System.out.println(k.getArvore());

    long elapsed = System.currentTimeMillis() - start;  
    
        System.out.println(elapsed);
    }  
}
