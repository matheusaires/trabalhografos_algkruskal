
package etapa3_kruskal;

import java.util.ArrayList;

public final class GrafoMA extends Grafo{
    
    private int[][] matriz;
    public  Aresta[] arestas;
    
    public GrafoMA(int n, Aresta[] a) {
        super(n);
        matriz = new int[n][n];
        arestas = a;
        
        for (Aresta aresta : arestas) {
            addAresta(aresta);
        }
        
    }
    
    @Override
    public void addAresta(Aresta a){
        if (existeVertice(a.getV1()) && existeVertice(a.getV2())){
            numArestas++;
            matriz[a.getV1()][a.getV2()] = 1;
            matriz[a.getV2()][a.getV1()] = 1;
        }
    }
    

    @Override
    public boolean rmAresta(int u, int v) {
        if(existeVertice(v) && existeVertice(u)){
            matriz[u-1][v-1] = 0;
            matriz[v-1][u-1] = 0;
            return true;
        }
        return false;
        
    }

    @Override
    public boolean addVertice() {
        int[][] aux = new int[numVertices+1][numVertices+1];
        for(int i=0; i<numVertices; i++){
            System.arraycopy(matriz[i], 0, aux[i], 0, numVertices);
        }
        numVertices++;
        matriz = aux;        
        return true;
    }

    @Override
    public boolean rmVertice(int v) {
        if(numVertices == 0){            
            return false;
        }
        v--;
        numVertices--;
        int[][] aux = new int[numVertices][numVertices];
        for(int i=0; i<numVertices; i++){
            for(int j=0; j<numVertices; j++){
                if (i>=v)
                    if(j>=v)
                        aux[i][j] = matriz[i+1][j+1];
                    else 
                        aux[i][j] = matriz[i+1][j];
                else
                    if(j>=v)
                        aux[i][j] = matriz[i][j+1];
                    else 
                        aux[i][j] = matriz[i][j];
            }                  
        }
      
        matriz = aux;
        
        return true;

    }

    @Override
    public ArrayList vizinhos(int v) {
        ArrayList vizinhos = new ArrayList();
        
        for(int i=0; i<numVertices; i++){
            if(matriz[v-1][i] == 1)
                vizinhos.add(i+1);
        }
        
        return vizinhos;
    }
    
    @Override
    public int getAresta(int v, int u){
        return matriz[v][u];
    };
    
    @Override
    public String toString() {
        String s = "MA";
        String l = "";
        for(int i=0; i<numVertices;i++){
            s = s+"| "+(i+1)+" ";
            l = l+"----";
        }
        l = l.substring(0,l.length()-1);
        
        s = s+"|\n   "+l+"\n";
        for(int i=0; i<numVertices; i++){
            s = s+" "+(i+1);
            for(int j=0; j<numVertices; j++){
                s = s+"| "+matriz[i][j]+" ";
            }
            s = s+"|\n   "+l+"\n";
        }
        return s;
    }
}
