/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etapa3_kruskal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Kruskal {
    private ArrayList list = new ArrayList();
    private final GrafoMA arvore;
    private final int subset[];
    private int v_set, u_set;
    
    public Kruskal(GrafoMA g){
        arvore = new GrafoMA(g.numVertices,new Aresta[0]);
        subset = new int[g.numVertices];
        for(int i=0; i<g.numVertices;i++){
            subset[i]= -1;
        }
        
        for(int i=0; i<g.numArestas;i++){
            list.add(g.arestas[i]);
        }
        
        Collections.sort(list,peso);
        
        for(int i=0; i<g.numArestas;i++){
            unir((Aresta)list.get(i));
        }
        
        
    }
    
    private int busca(int v){
        if(subset[v]==-1)
            return v;
        else
            return busca(subset[v]);
    }
    
    private boolean geraCiclo(){
        return (v_set == u_set);
    }
    
    private void unir(Aresta a){
        v_set = busca(a.getV1());
        u_set = busca(a.getV2());
        if (!geraCiclo()){
            subset[v_set] = u_set;
            arvore.addAresta(a);
        }     
    }
    
    public GrafoMA getArvore(){
        return arvore;
    }
    
    private final Comparator peso = (Comparator) (Object p1, Object p2) -> {
        int pp1 = ((Aresta) p1).getPeso();
        int pp2 = ((Aresta) p2).getPeso();
        return (int)Math.round(pp1-pp2);
    };
    
}
